package com.androidmastermodule.viewmodels;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;
import android.widget.RadioGroup;

import com.android.databinding.library.baseAdapters.BR;
import com.androidmastermodule.R;
import com.androidmastermodule.models.User;
import com.androidmastermodule.navigators.RegisterNavigator;

import java.util.Locale;

public class RegisterViewModel extends BaseObservable {
    private static final String TAG = RegisterViewModel.class.getSimpleName();
    private User user;
    private Context mContext;
    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";
    private RegisterNavigator mRegisterNavigator;
    @Bindable
    private String toastMessage = null;


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }

    public RegisterViewModel(Context mContext, RegisterNavigator mRegisterNavigator) {
        this.mContext = mContext;
        this.mRegisterNavigator = mRegisterNavigator;
        user = new User("", "", "", "", "", "", "", "", "");    }

    public void afterFnameTextChanged(CharSequence s) {
        user.setFname(s.toString());
    }

    public void afterLnameTextChanged(CharSequence s) {
        user.setLname(s.toString());
    }

    public void afterGenderTextChanged(RadioGroup radioGroup, int id) {
        switch (id) {
            case R.id.rbMale:
                user.setGender("Male");
                break;
            case R.id.rbFemale:
                user.setGender("Female");
                break;
            default:
                user.setGender("Male");
                break;

        }

    }

    public void afterBirthDateTextChanged(CharSequence s) {
        user.setBirthDate(s.toString());
    }

    public void afterPhoneTextChanged(CharSequence s) {
        user.setPhone(s.toString());
    }

    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }

    public void afterPasswordTextChanged(CharSequence s) {
        user.setPassword(s.toString());
    }

    public void onLoginClicked() {
        mRegisterNavigator.openLoginPage();
    }

    public void onForgotClick() {
        mRegisterNavigator.openForgotPassword();
    }

    public void onRegisterClicked() {
        Log.e(TAG, "onRegisterClicked: " + String.format(Locale.getDefault(), "%s\n%s\n%s\n%s\n%s\n%s\n%s", user.getFname(), user.getLname(), user.getBirthDate(), user.getGender(), user.getPhone(), user.getEmail(), user.getPassword()));

        if (!user.isFnameValid())
            setToastMessage(successMessage);
        else if (!user.isLnameValid())
            setToastMessage(successMessage);
        else if (!user.isGenderValid())
            setToastMessage(successMessage);
        else if (!user.isBirthDateValid())
            setToastMessage(successMessage);
        else if (!user.isPhoneValid())
            setToastMessage(successMessage);
        else if (!user.isEmailValid())
            setToastMessage(successMessage);
        else if (!user.isPasswordValid())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
    }
}

