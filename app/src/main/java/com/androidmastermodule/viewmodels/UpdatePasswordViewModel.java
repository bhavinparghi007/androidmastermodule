package com.androidmastermodule.viewmodels;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.androidmastermodule.models.User;
import com.androidmastermodule.navigators.UpdatePasswordNavigator;

import java.util.Locale;

public class UpdatePasswordViewModel extends BaseObservable {
    private User user;
    private Context mContext;
    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";
    private UpdatePasswordNavigator mUpdatePasswordNavigator;
    @Bindable
    public String toastMessage = null;


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }

    public UpdatePasswordViewModel(Context mContext, UpdatePasswordNavigator mUpdatePasswordNavigator) {
        this.mContext = mContext;
        this.mUpdatePasswordNavigator = mUpdatePasswordNavigator;
        user = new User("", "", "", "", "", "", "", "", "");
    }

    public void afterNewPasswordTextChanged(CharSequence s) {
        user.setNewPassword(s.toString());
    }

    public void afterConfirmPasswordTextChanged(CharSequence s) {
        user.setConfirmPassword(s.toString());
    }

    public void onUpdateClick() {
        Toast.makeText(mContext, String.format(Locale.getDefault(), "%s %s", user.getNewPassword(), user.getConfirmPassword()), Toast.LENGTH_SHORT).show();
        if (user.isNewPasswordValid())
            setToastMessage(successMessage);
        else if (user.isConfirmPasswordValid())
            setToastMessage(successMessage);
        else if (user.isConfirmPasswordMatch())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
    }

}

