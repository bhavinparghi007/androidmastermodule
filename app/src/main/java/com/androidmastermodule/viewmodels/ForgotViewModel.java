package com.androidmastermodule.viewmodels;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.androidmastermodule.models.User;
import com.androidmastermodule.navigators.ForgotNavigator;

import java.util.Locale;

public class ForgotViewModel extends BaseObservable {
    private User user;
    private Context mContext;
    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";
    private ForgotNavigator mForgotNavigator;
    @Bindable
    public String toastMessage = null;


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }

    public ForgotViewModel(Context mContext, ForgotNavigator mForgotNavigator) {
        this.mContext = mContext;
        this.mForgotNavigator = mForgotNavigator;
        user = new User("", "", "", "", "", "", "", "", "");
    }

    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }


    public void onLoginClicked() {
       mForgotNavigator.openLoginPage();
    }

    public void onForgotClick() {
        Toast.makeText(mContext, String.format(Locale.getDefault(), "%s", user.getEmail()), Toast.LENGTH_SHORT).show();
        if (user.isEmailValid())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
        
    }

}

