package com.androidmastermodule.viewmodels;


import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;
import com.androidmastermodule.models.User;
import com.androidmastermodule.navigators.LoginNavigator;
import com.androidmastermodule.networks.ApiCallBack;
import com.androidmastermodule.networks.ApiResponse;

import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends BaseObservable {
    private User user;
    private Context mContext;
    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";
    private LoginNavigator mLoginNavigator;
    private ApiCallBack mApiCallBack;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();
    @Bindable
    public String toastMessage = null;


    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
        notifyPropertyChanged(BR.toastMessage);
    }

    public LoginViewModel(Context mContext, LoginNavigator mLoginNavigator) {
        mApiCallBack = new ApiCallBack();
        this.mContext = mContext;
        this.mLoginNavigator = mLoginNavigator;
        user = new User("", "", "", "", "", "", "", "", "");
    }

    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }

    public void afterPasswordTextChanged(CharSequence s) {
        user.setPassword(s.toString());
    }
    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }
    public void onLoginClicked() {
        if (user.isEmailValid()) {
            Toast.makeText(mContext, "Please enter valid email", Toast.LENGTH_SHORT).show();
        } else if (user.isPasswordValid()) {
            Toast.makeText(mContext, "Please enter valid password", Toast.LENGTH_SHORT).show();
        } else {
            disposables.add(mApiCallBack.executeLogin(user.getEmail(), user.getPassword(),"Android","Android")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe((d) -> responseLiveData.setValue(ApiResponse.loading()))
                    .subscribe(result -> responseLiveData.setValue(ApiResponse.success(result)),
                            throwable -> responseLiveData.setValue(ApiResponse.error(throwable))));
        }
    }

    public void onForgotClick() {
        mLoginNavigator.openForgotPassword();
    }

    public void onRegisterClicked() {
        mLoginNavigator.openRegister();
    }
}

