package com.androidmastermodule.navigators;

public interface RegisterNavigator {
    public void openLoginPage();

    public void openForgotPassword();

}
