package com.androidmastermodule.navigators;

public interface LoginNavigator {
    public void openHomePage();

    public void openForgotPassword();

    public void openRegister();
}
