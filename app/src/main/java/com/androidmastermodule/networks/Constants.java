package com.androidmastermodule.networks;

class Constants {
    public static class ApiConstants {
        public static final String email="email";
        public static final String password="password";
        public static final String device_type="device_type";
        public static final String fire_base_token="fire_base_token";
    }
}
