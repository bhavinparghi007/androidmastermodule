package com.androidmastermodule.networks;

import com.google.gson.JsonElement;

import io.reactivex.Observable;

public class ApiCallBack {
    private ApiService apiCallInterface;

    public ApiCallBack() {
        this.apiCallInterface = ApiClient.getApiService();
    }

    /*
     * method to call login api
     * */
    public Observable<JsonElement> executeLogin(String email, String password,String device_type,String firebasetoken) {
        return apiCallInterface.doLogin(email, password,device_type,firebasetoken);
    }
}
