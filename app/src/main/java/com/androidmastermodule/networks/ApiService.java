package com.androidmastermodule.networks;

import com.google.gson.JsonElement;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @POST("users/login")
    Observable<JsonElement> doLogin(
            @Query(Constants.ApiConstants.email) String email,
            @Query(Constants.ApiConstants.password) String password,
            @Query(Constants.ApiConstants.device_type) String device_type,
            @Query(Constants.ApiConstants.fire_base_token) String fire_base_token
            );

}
