package com.androidmastermodule.models;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

public class User {


    @NonNull
    private String mFname;
    @NonNull
    private String mLname;
    @NonNull
    private String mGender;
    @NonNull
    private String mBirthDate;
    @NonNull
    private String mPhone;
    @NonNull
    private String mEmail;
    @NonNull
    private String mPassword;
    @NonNull
    private String mNewPassword;
    @NonNull
    private String mConfirmPassword;

    public User(@NonNull String fname, @NonNull String lname, @NonNull String gender, @NonNull String birthdate, @NonNull String phone, @NonNull final String email, @NonNull final String password, @NonNull String newpassword, @NonNull String confirmpassword) {
        mFname = fname;
        mLname = lname;
        mGender = gender;
        mBirthDate = birthdate;
        mPhone = phone;
        mEmail = email;
        mPassword = password;
        mNewPassword = newpassword;
        mConfirmPassword = confirmpassword;
    }


    //get methods

    @NonNull
    public String getEmail() {
        return mEmail;
    }


    @NonNull
    public String getPassword() {
        return mPassword;
    }

    @NonNull
    public String getFname() {
        return mFname;
    }

    @NonNull
    public String getLname() {
        return mLname;
    }

    @NonNull
    public String getGender() {
        return mGender;
    }

    @NonNull
    public String getBirthDate() {
        return mBirthDate;
    }

    @NonNull
    public String getPhone() {
        return mPhone;
    }

    @NonNull
    public String getNewPassword() {
        return mNewPassword;
    }

    @NonNull
    public String getConfirmPassword() {
        return mConfirmPassword;
    }

    //set methods
    public void setPassword(@NonNull final String password) {
        mPassword = password;
    }

    public void setEmail(@NonNull final String email) {
        mEmail = email;
    }

    public void setFname(@NonNull String mFname) {
        this.mFname = mFname;
    }

    public void setLname(@NonNull String mLname) {
        this.mLname = mLname;
    }

    public void setGender(@NonNull String mGender) {
        this.mGender = mGender;
    }

    public void setBirthDate(@NonNull String mBirthDate) {
        this.mBirthDate = mBirthDate;
    }

    public void setPhone(@NonNull String mPhone) {
        this.mPhone = mPhone;
    }

    public void setNewPassword(@NonNull String mNewPassword) {
        this.mNewPassword = mNewPassword;
    }

    public void setConfirmPassword(@NonNull String mConfirmPassword) {
        this.mConfirmPassword = mConfirmPassword;
    }
    //validate methods

    public boolean isEmailValid() {
        return TextUtils.isEmpty(getEmail()) || !Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches();
    }

    public boolean isPasswordValid() {
        return TextUtils.isEmpty(getPassword()) || getPassword().length() < 5;
    }

    public boolean isNewPasswordValid() {
        return TextUtils.isEmpty(getNewPassword()) || getNewPassword().length() < 5;
    }

    public boolean isConfirmPasswordValid() {
        return TextUtils.isEmpty(getConfirmPassword()) || getConfirmPassword().length() < 5;
    }

    public boolean isConfirmPasswordMatch() {
        return getNewPassword().equalsIgnoreCase(getConfirmPassword());
    }

    public boolean isPhoneValid() {
        return TextUtils.isEmpty(getPhone()) || getPhone().length() < 5;
    }


    public boolean isFnameValid() {
        return TextUtils.isEmpty(getFname()) || getFname().length() < 3;
    }

    public boolean isLnameValid() {
        return TextUtils.isEmpty(getLname()) || getLname().length() < 3;
    }

    public boolean isGenderValid() {
        return TextUtils.isEmpty(getGender());
    }

    public boolean isBirthDateValid() {
        return TextUtils.isEmpty(getBirthDate());
    }


}
