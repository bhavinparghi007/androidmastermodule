package com.androidmastermodule.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidmastermodule.R;
import com.androidmastermodule.databinding.ActivityForgotBinding;
import com.androidmastermodule.databinding.ActivityLoginBinding;
import com.androidmastermodule.navigators.ForgotNavigator;
import com.androidmastermodule.viewmodels.ForgotViewModel;
import com.androidmastermodule.viewmodels.LoginViewModel;

public class ForgotActivity extends AppCompatActivity implements ForgotNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityForgotBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot);
        activityMainBinding.setViewModel(new ForgotViewModel(this, this));
        activityMainBinding.executePendingBindings();
    }

    @Override
    public void openLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        finish();
        startActivity(intent);
    }
}
