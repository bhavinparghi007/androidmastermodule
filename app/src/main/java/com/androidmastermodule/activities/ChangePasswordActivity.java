package com.androidmastermodule.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.androidmastermodule.R;
import com.androidmastermodule.databinding.ActivityChangePasswordBinding;
import com.androidmastermodule.navigators.ChangePasswordNavigator;
import com.androidmastermodule.viewmodels.ChangePasswordViewModel;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityChangePasswordBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_update_password);
        activityMainBinding.setViewModel(new ChangePasswordViewModel(this, this));
        activityMainBinding.executePendingBindings();
    }


    @Override
    public void openHomePage() {

    }
}
