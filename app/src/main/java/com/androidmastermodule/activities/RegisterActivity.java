package com.androidmastermodule.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.androidmastermodule.R;
import com.androidmastermodule.databinding.ActivityRegisterBinding;
import com.androidmastermodule.navigators.RegisterNavigator;
import com.androidmastermodule.viewmodels.RegisterViewModel;

public class RegisterActivity extends AppCompatActivity implements RegisterNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRegisterBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        activityMainBinding.setViewModel(new RegisterViewModel(this, this));
        activityMainBinding.executePendingBindings();
    }

    @Override
    public void openLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void openForgotPassword() {
        Intent intent = new Intent(this, ForgotActivity.class);
        finish();
        startActivity(intent);
    }
}
