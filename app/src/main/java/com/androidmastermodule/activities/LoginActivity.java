package com.androidmastermodule.activities;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidmastermodule.R;
import com.androidmastermodule.databinding.ActivityLoginBinding;
import com.androidmastermodule.navigators.LoginNavigator;
import com.androidmastermodule.networks.ApiResponse;
import com.androidmastermodule.viewmodels.LoginViewModel;
import com.google.gson.JsonElement;

public class LoginActivity extends AppCompatActivity implements LoginNavigator, Observer<ApiResponse> {

    private LoginViewModel viewModel;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBind();
       initDialog();

    }

    private void initDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getString(R.string.progressMessage));
    }

    private void initBind() {
        ActivityLoginBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = new LoginViewModel(this, this);
        activityMainBinding.setViewModel(viewModel);
        activityMainBinding.executePendingBindings();
        viewModel.loginResponse().observe(this, this);
    }

    @Override
    public void openHomePage() {

    }

    @Override
    public void openForgotPassword() {
        Intent intent = new Intent(this, ForgotActivity.class);
        startActivity(intent);
    }

    @Override
    public void openRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onChanged(@Nullable ApiResponse apiResponse) {
        switch (apiResponse.status) {

            case LOADING:
                progressDialog.show();
                break;

            case SUCCESS:
                progressDialog.dismiss();
                renderSuccessResponse(apiResponse.data);
                break;

            case ERROR:
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this,getResources().getString(R.string.errorString), Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }
    private void renderSuccessResponse(JsonElement response) {
        if (!response.isJsonNull()) {
            Log.d("response=", response.toString());
        } else {
            Toast.makeText(LoginActivity.this,getResources().getString(R.string.errorString), Toast.LENGTH_SHORT).show();
        }
    }
}
