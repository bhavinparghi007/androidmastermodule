package com.androidmastermodule.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.androidmastermodule.R;
import com.androidmastermodule.databinding.ActivityUpdatePasswordBinding;
import com.androidmastermodule.navigators.UpdatePasswordNavigator;
import com.androidmastermodule.viewmodels.UpdatePasswordViewModel;

public class UpdatePasswordActivity extends AppCompatActivity implements UpdatePasswordNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUpdatePasswordBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_update_password);
        activityMainBinding.setViewModel(new UpdatePasswordViewModel(this, this));
        activityMainBinding.executePendingBindings();
    }

    @Override
    public void openLoginPage() {

    }
}
